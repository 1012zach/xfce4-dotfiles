# Dotfiles

This is the dotfiles for my XFCE 4 rice, enjoy! (no seriously, enjoy it)

[![https://imgur.com/DxrACuY.png](https://imgur.com/DxrACuY.png)](https://imgur.com/DxrACuY.png)

> Dependencies

xfce4

xfce4-appmenu-plugin

plank

xfce4-panel-profiles

> How to install

Copy the icons themes from icons into `/usr/share/icons`.

This will install the Papirus icon theme.

Open Settings Manager and select "Appearance" and go to the Icons tab and change to the Paprius-Dark Icon theme

Copy the themes from themes into either `~/.themes` or `/usr/share/themes`

Open Settings Manager and click "Appancence" and select `ZorinBlue-Dark`.

Now click "All Settings" at the bottom of the Window and select "Window Manager". This will open Window Manager settings
Here select ZorinBlue-Dark as the Window Manager theme. Also remove all buttons except for the Close, Minimise, and Maximise buttons. The Window Buttons need to be moved the the left and need to be in this order:

`Close - Minimise - Maximise`

Open Panel Preferences and click "Backup and Restore". This will open another Window. Click on the Import button and go where you cloned the repository and select "xfce-panels-config.tar.bz2". When you do it will ask you if you want to backup your current setup, click no unless you want to save your current panel setup.


Now lets install the Plank themes

Copy the Plank themes from plank/themes/ of this repository and place them in /usr/share/plank/themes 

Next, Ctrl + Right Click on your plank and click "Preferences". Now select either "Azeny", "Azeny Small", or "Azeny Big" as your plank dock theme.

We are almost done!

Copy the wallpaper from this repository and place it in ~/Pictures or where ever you put your wallpapers and set it as your wallpaper.

Congrats! You now successly applied my rice! If you had any trouble installing or want to leave any suggestions, feel free to make a issue.
